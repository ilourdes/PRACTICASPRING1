package com.example.demo.dominio;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by CLIENTE on 19/07/2017.
 */
public interface EstudianteRepository  extends JpaRepository<Estudiante, Long>{
}
