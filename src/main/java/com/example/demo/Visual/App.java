package com.example.demo.Visual;

import com.example.demo.dominio.Estudiante;
import com.example.demo.dominio.EstudianteRepository;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.criteria.CriteriaBuilder;




/**
 * Created by CLIENTE on 19/07/2017.
 */


@SpringUI
public class App extends UI {
    @Autowired
    EstudianteRepository estudianteRepository;



    protected void init(VaadinRequest vaadinRequest){
        VerticalLayout layout = new VerticalLayout();
        HorizontalLayout hlayout = new HorizontalLayout();
        Grid<Estudiante> grid=new Grid<>();

        TextField nombre=new TextField("Nombre");
        TextField edad=new TextField("Edad");

        Button add=new Button("Adicionar");
        add.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Estudiante e=new Estudiante();

                e.setNombre(nombre.getValue());
                e.setEdad(Integer.parseInt(edad.getValue()));
                estudianteRepository.save(e);
                grid.setItems(estudianteRepository.findAll());
                nombre.clear();
                edad.clear();
                Notification.show("Estudiante adicionado");

            }
        });

        hlayout.addComponents(nombre, edad, add);
        hlayout.setComponentAlignment(add, Alignment.BOTTOM_RIGHT);


        grid.addColumn(Estudiante::getId).setCaption("Id");
        grid.addColumn(Estudiante::getNombre).setCaption("Nombre");
        grid.addColumn(Estudiante::getEdad).setCaption("Edad");

        layout.addComponents(hlayout);
        layout.addComponents(grid);
        

            setContent( layout );//
        
    }




}

